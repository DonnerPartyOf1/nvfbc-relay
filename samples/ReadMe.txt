
/*
 *
 * Copyright 1993-2018 NVIDIA Corporation.  All rights reserved.
 * NOTICE TO LICENSEE: This source code and/or documentation ("Licensed Deliverables")
 * are subject to the applicable NVIDIA license agreement
 * that governs the use of the Licensed Deliverables.
 *
 */

NvFBC Dependencies
------------------------
- CUDA Toolkit version 6.5 or newer
- Microsoft DirectX SDK (June 2010)
- Driver: Refer NVIDIA Capture SDK release notes

NVIDIA Capture Sample Dependencies
-------------------------------
- NvFBC samples require DX9


Install Notes (9/24/2018)
-------------------------

1. High-end Quadro cards, GRID, TESLA boards only. e.g. Quadro K5000, M5000

2. Use Latest Driver, as per NVIDIA Capture SDK release notes

3. Install CUDA 6.5 Toolkit

4. Install installer .msi package

5. Run as administrator: 
    > cd\Program Files(x86)\NVIDIA Corporation\NVIDIA Capture SDK\bin
	> NvFBCEnable -enable

6. Build and Run samples.
